#include "TimerOne.h"

//ELIMINA BOUNCING
int L1 = 11;
int L2 = 12;
int L3 = 13;
int LB = 10;
int LR = 9;
int Ts = 2;
int Td = 3;
#define POT_PIN A0
bool up=true;
bool start=true;
int firstLed;
int score=0;
int brightness = 0;    
int fadeAmount = 5;    

//FILE.H DA FARE
void setup() {
  // put your setup code here, to run once:
  pinMode(L1, OUTPUT);
  pinMode(L2, OUTPUT);
  pinMode(L3, OUTPUT);
  pinMode(Ts, INPUT);
  pinMode(Td, INPUT);
  attachInterrupt(digitalPinToInterrupt(Ts), play, RISING);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(start) {
    slampeggia();
  }
  noInterrupts();
  digitalWrite(firstLed,HIGH);
  interrupts();
  attachInterrupt(digitalPinToInterrupt(Td), moveLed, RISING);
}


void slampeggia(){
    if(up) {
      digitalWrite(L1, HIGH);
      delay(200);
      digitalWrite(L1, LOW);
     digitalWrite(L2, HIGH);
     delay(200);
      digitalWrite(L2, LOW);
      up = false;
    }
      else{
         digitalWrite(L3, HIGH);
      delay(200);
      digitalWrite(L3, LOW);
      digitalWrite(L2, HIGH);
     delay(200);
      digitalWrite(L2, LOW);
      up=true;
      }
}


void play(){
  if(start){
    start=false;  
    restartGame();
    
  }
}
void randomLed(){
  int rnd = random(1,4);
    if(rnd==1){
      firstLed=L1;
    }
    if(rnd==2){
      firstLed=L2;
    }
    if(rnd==3){
      firstLed=L3;
    }
}

void moveLed(){ 
  if(firstLed==L1){
    digitalWrite(L1, LOW);
    firstLed=LB;
    pulse();
  } else if(firstLed==L2){
    digitalWrite(L2, LOW);
    digitalWrite(L1, HIGH);
    firstLed=L1;

  } else if(firstLed==L3){
    digitalWrite(L3, LOW);
    digitalWrite(L2, HIGH);
    firstLed=L2;
  } else {
    digitalWrite(LR, HIGH);
    firstLed=LR;
  }
}


void pulse() {
  int i = 0;
  while(i<100){
    noInterrupts();
    if(firstLed==LR){
      gameOver();
      return;
    }
    interrupts();
    analogWrite(LB, brightness);      
    // change the brightness for next time through the loop: 
    brightness = brightness + fadeAmount;  
    // reverse the direction of the fading at the ends of the fade:   
    if (brightness == 0 || brightness == 255) { 
      fadeAmount = -fadeAmount ;  
    }     
    // wait for 30 milliseconds to see the dimming effect      
    delay(30);
    i++;  
  }
  analogWrite(LB, 0);     
  restartGame();
  
}

void gameOver(){
  analogWrite(LB,0);
  //Cambia e controlla stampa
  Serial.println("Game Over - Score: X");
}

void restartGame(){
  randomLed();
   
}
